#include <Python.h>


// Simple return of a string
//{{{ simple()
static PyObject* simple( PyObject* self, PyObject* args, PyObject* kwds ){
  return Py_BuildValue( "s", "Simple Python Extension" );
}
//}}}

// Simple return of int, double and string
//{{{ simple_parsetuple()
static PyObject* simple_parsetuple( PyObject* self, PyObject* args, PyObject* kwds ){
  int i    = 0;
  double d = 0.0;
  char *s;
  if (!PyArg_ParseTuple( args, "ids", &i, &d, &s)){
   return NULL;
  }

 PyObject* rtn = Py_BuildValue( "ids", i, d, s );
 return rtn;

}
//}}}
//{{{ simple_parsedict()
static PyObject* simple_parsedict( PyObject* self, PyObject* args, PyObject* kwds ){
  PyObject* dict;
  if (!PyArg_ParseTuple( args, "O!", &PyDict_Type, &dict )){
   return NULL;
  }
  
  PyObject* keys = PyDict_Keys( dict );
  PyObject* vals = PyDict_Values( dict );
  printf( "Number of keys returned %d\n", (int)PyList_Size( keys ));
  for ( int k = 0; k < PyList_Size(keys); ++k ){
    PyObject* key = PyList_GetItem( keys, k );
    PyObject* val = PyList_GetItem( vals, k );
    // This doesn't work right if your value is something other than a string
    printf( "key/val = %s, %s\n", PyString_AsString( key ), PyString_AsString( val ));
   
    // so this forces the conversion to a string for display purposes 
    PyObject* key_o = PyObject_Repr( key );
    PyObject* val_o = PyObject_Repr( val );
    printf( "key/val = %s, %s\n", PyString_AsString( key_o ), PyString_AsString( val_o ));
  }

 return Py_None;

}
//}}}
//{{{ simple_writedict()
static PyObject* simple_writedict( PyObject* self, PyObject* args, PyObject* kwds ){
  PyObject *dict = PyDict_New();
  PyObject* key = Py_BuildValue("s", "simple" );
  PyObject* val = Py_BuildValue("d", 10.0 );
  PyDict_SetItem( dict, key, val );
  return dict;
}

//}}}
static char simple_docs[] =
  "simple(): we are testing this interface\n";

// {{{ simple_funcs()
static PyMethodDef simple_funcs[] = {
  {"simple", (PyCFunction)simple, METH_VARARGS|METH_KEYWORDS, simple_docs},
  {"simple_parsetuple", (PyCFunction)simple_parsetuple, METH_VARARGS|METH_KEYWORDS, simple_docs},
  {"simple_parsedict", (PyCFunction)simple_parsedict, METH_VARARGS|METH_KEYWORDS, simple_docs},
  {"simple_writedict", (PyCFunction)simple_writedict, METH_VARARGS|METH_KEYWORDS, simple_docs},
  {NULL, NULL, 0, NULL }
};
//}}}

//{{{ initsimple
void initsimple(void){
  Py_InitModule3( "simple", simple_funcs,
                  "Simple Extension module");
}
//}}}

//{{{ These are the PyArg_ParseTuple Format strings for easy reference
/* 
c      char                A Python string of length 1 becomes a C char.
d	     double	             A Python float becomes a C double.
f	     float	             A Python float becomes a C float.
i	     int	               A Python int becomes a C int.
l	     long	               A Python int becomes a C long.
L	     long long	         A Python int becomes a C long long
O	     PyObject*	         Gets non-NULL borrowed reference to Python argument.
s	     char*	             Python string without embedded nulls to C char*.
s#	   char*+int	         Any Python string to C address and length.
t#	   char*+int	         Read-only single-segment buffer to C address and length.
u	     Py_UNICODE*         Python Unicode without embedded nulls to C.
u#	   Py_UNICODE*+int	   Any Python Unicode C address and length.
w#	   char*+int	         Read/write single-segment buffer to C address and length.
z	     char*	             Like s, also accepts None (sets C char* to NULL).
z#     char*+int	         Like s#, also accepts None (sets C char* to NULL).
(...)	as per ...	         A Python sequence is treated as one argument per item.
|	 	                       The following arguments are optional.
:	 	                       Format end, followed by function name for error messages.
;	 	                       Format end, followed by entire error message text.
*/
//}}}
